package wordchains.tests;

import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import wordchains.WordChains;

/**
 * WordChains Generator test file
 * @author Grzegorz Rzepka
 */
public class WordChainsTest {
    
    WordChains wordChains = new WordChains();
    
    public WordChainsTest() {
    }
    
    @Test
    public void testIsOneLetterDiff() {
        assertTrue(wordChains.isOneLetterDiff("cat", "cad"));
    }
    
    @Test
    public void testFormatResult() {
        Map<String, String> testMap = new HashMap<>();
        testMap.put("cot", "cat");
        testMap.put("cog", "cot");
        testMap.put("dog", "cog");
        
        Assert.assertEquals(wordChains.formatResult("dog", testMap), "cat, cot, cog, dog");
    }
}
