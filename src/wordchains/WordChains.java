package wordchains;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * WordChains Generator
 * @author Grzegorz Rzepka
 */
public class WordChains {
    
    private static final String NOT_THE_SAME_LENGTH_EXCEPTION_TEXT = "Start string must be the same length as end string";
    private static final String WORD_NOT_FOUND_IN_DICTIONARY_EXCEPTION_TEXT = "Input word not found in dictionary";
    
    public static void main(String[] args) {
        WordChains wordChains = new WordChains();
        
        try {
            String filename = "wordlist.txt";
            System.out.println(wordChains.generateChain(filename, "ruby", "code"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private List<String> readWords(String filename, String startWord) throws IOException {
        List<String> resultList = new ArrayList<>();
        try (BufferedReader r = new BufferedReader(new FileReader(filename))) {
            String currentLine;
            
            while((currentLine = r.readLine()) != null) {
                if(currentLine.length() == startWord.length()) {
                    resultList.add(currentLine);
                }
            }
        }
        return resultList;
    }
    
    private String generateChain(String filename, String chainStart, String chainEnd) throws IOException {
        if(chainStart.length() != chainEnd.length()) {
            throw new RuntimeException(NOT_THE_SAME_LENGTH_EXCEPTION_TEXT);
        }
        
        if(chainStart.equals(chainEnd)) {
            return chainStart;
        }
        
        List<String> wordsFromDic = readWords(filename, chainStart);
        
        if (!wordsFromDic.contains(chainStart)) {
            throw new RuntimeException(WORD_NOT_FOUND_IN_DICTIONARY_EXCEPTION_TEXT + ": " + chainStart);
        }
        
        if(!wordsFromDic.contains(chainEnd)) {
            throw new RuntimeException(WORD_NOT_FOUND_IN_DICTIONARY_EXCEPTION_TEXT + ": " + chainEnd);
        }
        
        wordsFromDic.remove(chainStart);
        Map<String, String> resultMap = new HashMap<>();//current -> previous
        List<String> usedWords = new ArrayList<>();
        List<String> tmpList = new ArrayList<>();
        tmpList.add(chainStart);
        
        while(wordsFromDic.size() > 0) {
            for(String wordFromDic : wordsFromDic) {
                for(String tmp : tmpList) {
                    if(isOneLetterDiff(wordFromDic, tmp)) {
                        resultMap.put(wordFromDic, tmp);
                        usedWords.add(wordFromDic);
                        if(wordFromDic.equals(chainEnd)) {
                            return formatResult(wordFromDic, resultMap);
                        }
                    }
                }
            }
            tmpList.clear();
            tmpList.addAll(usedWords);
            wordsFromDic.removeAll(usedWords);
        }
        
        return "Chain not found!";
    }

    public String formatResult(String word, Map<String, String> resultMap) {
        String result = word;
        word = resultMap.get(word);
        
        while (word != null) {
            result = word + ", " + result;
            word = resultMap.get(word);
        }
        
        return result;
    }

    public boolean isOneLetterDiff(String first, String second) {
        boolean differentFound = false;
        
        for (int i = 0; i < first.length(); i++) {
            if (first.charAt(i) != second.charAt(i)) {
                if(differentFound == false) {
                    differentFound = true;
                } else {
                    return false;
                }
            }
        }
        
        return true;
    }
    
}
